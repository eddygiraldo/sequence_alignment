SeqAligApp.controller('SequenceAlignmentController', function ($scope, $timeout) {

    $scope.Home = {
        Title: "Alineamiento de secuencias",
    }

    $scope.ocultarProcedimiento = true;
    $scope.bloqueoCalculo = true;
    $scope.bloqueoProcedimiento = true;

    $scope.Text = "Esperando carga de archivo FASTA";
    $scope.SequenceOne = "";
    $scope.SequenceTwo = "";
    $scope.SequenceOneNbr = "";
    $scope.SequenceTwoNbr = "";

    $scope.Conversion = [
        { id: 1, value: "A" },
        { id: 2, value: "G" },
        { id: 3, value: "C" },
        { id: 4, value: "T" }
    ]

    $scope.Gap = -5;

    $scope.MatrizPuntaje = [
        {
            row: 0,
            columns:
                [{ column: 0, value: "-" },
                { column: 1, value: 1 },
                { column: 2, value: 2 },
                { column: 3, value: 3 },
                { column: 4, value: 4 }]
        },

        {
            row: 1,
            columns:
                [{ column: 0, value: 1 },
                { column: 1, value: 10 },
                { column: 2, value: -1 },
                { column: 3, value: -3 },
                { column: 4, value: -4 }]
        },

        {
            row: 2,
            columns:
                [{ column: 0, value: 2 },
                { column: 1, value: -1 },
                { column: 2, value: 7 },
                { column: 3, value: -5 },
                { column: 4, value: -3 }]
        },

        {
            row: 3,
            columns:
                [{ column: 0, value: 3 },
                { column: 1, value: -3 },
                { column: 2, value: -5 },
                { column: 3, value: 9 },
                { column: 4, value: 0 }]
        },

        {
            row: 4,
            columns:
                [{ column: 0, value: 4 },
                { column: 1, value: -4 },
                { column: 2, value: -3 },
                { column: 3, value: 0 },
                { column: 4, value: 8 }]
        },
    ]

    $scope.Matriz = [];
    $scope.SecuenciaAlineada = [];

    function handleFileSelect(evt) {

        evt.stopPropagation();
        evt.preventDefault();
        var file = evt.dataTransfer.files[0]; // FileList object.;

        var reader = new FileReader();
        reader.onload = function (progressEvent) {
            // Entire file
            console.log(this.result);
            $scope.Text = this.result;

        };
        reader.readAsText(file);
    }

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    // Setup the dnd listeners.
    var dropZone = document.getElementById('drop_zone');
    dropZone.addEventListener('dragover', handleDragOver, false);
    dropZone.addEventListener('drop', handleFileSelect, false);

    //Extraer secuencias 
    $scope.ExtractSequences = function () {
        $scope.SequenceOne = "";
        $scope.SequenceTwo = "";
        var linesFASTA = $scope.Text.split("\n");
        var line = 0;
        for (var i = 0; i < linesFASTA.length; i++) {
            if (linesFASTA[i].includes(">")) {
                line++;
            } else {
                if (line === 1) {
                    $scope.SequenceOne += linesFASTA[i].replace("\r", "");
                } else {
                    $scope.SequenceTwo += linesFASTA[i].replace("\r", "");
                }
            }
        }

        $scope.bloqueoCalculo = false;
    }

    $scope.filterHead = function (item) {
        return item.row === 0;
    };

    $scope.filterBody = function (item) {
        return item.row !== 0;
    };

    $scope.Procedimiento = function (item) {
        if ($scope.ocultarProcedimiento) {
            $scope.ocultarProcedimiento = false;
        }
        else {
            $scope.ocultarProcedimiento = true;
        }

    };

    $scope.CalcularMatriz = function () {
        $scope.Matriz = [];
        $scope.SequenceOneNbr = "";
        $scope.SequenceTwoNbr = "";
        //i -> fila
        //j -> columna
        $scope.Matriz.push({ row: 0, columns: [{ column: 0, value: "" }] });
        $scope.Matriz.filter(e => e.row == 0).map(e => e.columns)[0].push({ column: 1, value: "" });
        $scope.Matriz.push({ row: 1, columns: [{ column: 0, value: "" }] });


        for (var i = 0; i < $scope.SequenceOne.length; i++) {
            var x = "";
            x = $scope.Conversion.filter(e => e.value == $scope.SequenceOne[i]).map(e => e.id)
            $scope.Matriz.filter(e => e.row == 0).map(e => e.columns)[0].push({ column: i + 2, value: x[0] });
            $scope.SequenceOneNbr += x[0];
        }

        for (var j = 0; j < $scope.SequenceTwo.length; j++) {
            var x = "";
            x = $scope.Conversion.filter(e => e.value == $scope.SequenceTwo[j]).map(e => e.id)
            $scope.Matriz.push({ row: (j + 2), columns: [{ column: 0, value: x[0] }] });
            $scope.SequenceTwoNbr += x[0];
        }

        var valor1 = 0;
        var valor2 = 0;
        var valor3 = 0;
        var up = 0;
        var left = 0;
        var diagonal = 0;
        var gap = $scope.Gap;
        var secuenceTop = 0;
        var secuenceLeft = 0;
        //i -> fila
        //j -> columna
        for (var j = 1; j <= $scope.SequenceTwoNbr.length + 1; j++) {
            for (var i = 1; i <= $scope.SequenceOneNbr.length + 1; i++) {
                if (i > 1) {
                    left = $scope.Matriz.filter(e => e.row == (j)).map(e => e.columns)[0].filter(e => e.column == (i - 1)).map(e => e.value)[0];
                } else {
                    left = 0;
                }

                if (j > 1) {
                    up = $scope.Matriz.filter(e => e.row == (j - 1)).map(e => e.columns)[0].filter(e => e.column == (i)).map(e => e.value)[0];
                } else {
                    up = 0;
                }

                if (i >= 2 && j >= 2) {
                    diagonal = $scope.Matriz.filter(e => e.row == (j - 1)).map(e => e.columns)[0].filter(e => e.column == (i - 1)).map(e => e.value)[0];
                }


                if (j == 1) {
                    if (i == 1) {
                        $scope.Matriz.filter(e => e.row == j).map(e => e.columns)[0].push({ column: i, value: 0 });
                    } else {

                        $scope.Matriz.filter(e => e.row == j).map(e => e.columns)[0].push({ column: i, value: (left + gap) });
                    }
                }
                else if (i == 1) {
                    $scope.Matriz.filter(e => e.row == j).map(e => e.columns)[0].push({ column: i, value: (up + gap) });
                } else {
                    secuenceTop = $scope.Matriz.filter(e => e.row == (0)).map(e => e.columns)[0].filter(e => e.column == (i)).map(e => e.value)[0];
                    secuenceLeft = $scope.Matriz.filter(e => e.row == (j)).map(e => e.columns)[0].filter(e => e.column == (0)).map(e => e.value)[0];

                    valor1 = up + gap;
                    valor2 = left + gap;
                    valor3 = diagonal + $scope.MatrizPuntaje.filter(e => e.row == secuenceTop).map(e => e.columns)[0].filter(e => e.column == secuenceLeft).map(e => e.value)[0];

                    $scope.Matriz.filter(e => e.row == j).map(e => e.columns)[0].push({ column: i, value: Math.max(valor1, valor2, valor3) });
                }
            }
        }

        //console.log($scope.Matriz);
        $scope.SequencesAlignment();

    }


    $scope.SequencesAlignment = function () {
        $scope.SecuenciaAlineada = [];
        var j = $scope.SequenceTwoNbr.length + 1;
        var i = $scope.SequenceOneNbr.length + 1;
        var len = $scope.SequenceOneNbr.length;
        var gap = $scope.Gap;

        var up = 0;
        var left = 0;
        var diagonal = 0;
        var valor1 = 0;
        var valor2 = 0;
        var valor3 = 0;
        var valorMatriz = 0;

        var secuenceTop = 0;
        var secuenceLeft = 0;

        var a = "";
        var b = "";
        var finish = false

        while (!finish) {
            left = $scope.Matriz.filter(e => e.row == (j)).map(e => e.columns)[0].filter(e => e.column == (i - 1)).map(e => e.value)[0];
            up = $scope.Matriz.filter(e => e.row == (j - 1)).map(e => e.columns)[0].filter(e => e.column == (i)).map(e => e.value)[0];
            diagonal = $scope.Matriz.filter(e => e.row == (j - 1)).map(e => e.columns)[0].filter(e => e.column == (i - 1)).map(e => e.value)[0];

            valorMatriz = $scope.Matriz.filter(e => e.row == (j)).map(e => e.columns)[0].filter(e => e.column == (i)).map(e => e.value)[0];

            secuenceTop = $scope.Matriz.filter(e => e.row == (0)).map(e => e.columns)[0].filter(e => e.column == (i)).map(e => e.value)[0];
            secuenceLeft = $scope.Matriz.filter(e => e.row == (j)).map(e => e.columns)[0].filter(e => e.column == (0)).map(e => e.value)[0];


            valor1 = up + gap;
            valor2 = left + gap;
            valor3 = diagonal + $scope.MatrizPuntaje.filter(e => e.row == secuenceTop).map(e => e.columns)[0].filter(e => e.column == secuenceLeft).map(e => e.value)[0];

            //le -> sequence two
            b = $scope.Matriz.filter(e => e.row == (j)).map(e => e.columns)[0].filter(e => e.column == (0)).map(e => e.value)[0];
            b = $scope.Conversion.filter(e => e.id == b).map(e => e.value)[0];
            //up -> sequence one
            a = $scope.Matriz.filter(e => e.row == (0)).map(e => e.columns)[0].filter(e => e.column == (i)).map(e => e.value)[0];
            a = $scope.Conversion.filter(e => e.id == a).map(e => e.value)[0];

            //le - gap
            if (valor1 == valorMatriz) {
                $scope.SecuenciaAlineada.push({ id: len, A: "-", B: b, UP: up, LEFT: left, DIAG: diagonal, ELEM: valorMatriz });
                j -= 1;
                if (up == 0 && j == 1) {
                    finish = true;
                }
                //up - gap
            } else if (valor2 == valorMatriz) {
                $scope.SecuenciaAlineada.push({ id: len, A: a, B: "-", UP: up, LEFT: left, DIAG: diagonal, ELEM: valorMatriz });
                i -= 1;
                if (left == 0 && i == 1) {
                    finish = true;
                }
                //equals
            } else if (valor3 == valorMatriz) {
                $scope.SecuenciaAlineada.push({ id: len, A: a, B: b, UP: up, LEFT: left, DIAG: diagonal, ELEM: valorMatriz });
                i -= 1;
                j -= 1;
                if (diagonal == 0 && j == 1 && i == 1) {
                    finish = true;
                }
            }

            len -= 1;
        }

        console.log($scope.SecuenciaAlineada);
        $scope.bloqueoProcedimiento = false;

    }


});

