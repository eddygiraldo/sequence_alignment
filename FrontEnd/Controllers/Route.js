var SeqAligApp = angular.module('XORApp', ['ngRoute']);

SeqAligApp.controller('MainController', function($scope, $route, $routeParams, $location) {
        $scope.$route = $route;
        $scope.$location = $location;
        $scope.$routeParams = $routeParams;
    })

    SeqAligApp.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
        templateUrl: '../View/home.html',
        controller: 'HomeController'
    })
    .when('/SequenceAlignment', {
        templateUrl: '../View/SequenceAlignment/sequenceAlignment.html',
        controller: 'SequenceAlignmentController'
    })    
    .otherwise({
        redirectTo: '/'
    });

    //$locationProvider.html5Mode(true);
});